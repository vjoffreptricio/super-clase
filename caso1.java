class Empleado {
    private String primerNombre;
    private String apellidoMaterno;	
    private String numeroSeguridadSocial;
    private Double ventasBrutas;

    public Empleado(String primerNombre, String apellidoMaterno, String numeroSeguridadSocial, Double ventasBrutas) {
        this.primerNombre = primerNombre;
        this.apellidoMaterno = apellidoMaterno;
        this.numeroSeguridadSocial = numeroSeguridadSocial;
        this.ventasBrutas = ventasBrutas;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNumeroSeguridadSocial() {
        return numeroSeguridadSocial;
    }

    public void setNumeroSeguridadSocial(String numeroSeguridadSocial) {
        this.numeroSeguridadSocial = numeroSeguridadSocial;
    }

    public Double getVentasBrutas() {
        return ventasBrutas;
    }

    public void setVentasBrutas(Double ventasBrutas) {
        this.ventasBrutas = ventasBrutas;
    }
    
    @Override
    public String toString() {
        return "Empleado{" + "Nombre=" + getPrimerNombre() + ", Apellido=" + apellidoMaterno + ", Seguridad Social=" + numeroSeguridadSocial + ", Ventas brutas=" + ventasBrutas +  '}';
    }

    
}
 class EmpleadoPorComision extends Empleado{
    
    private Double tarifaComision;

    public EmpleadoPorComision(String primerNombre, String apellidoMaterno, String numeroSeguridadSocial, Double ventasBrutas,Double tarifaComision) {
        super(primerNombre, apellidoMaterno, numeroSeguridadSocial, ventasBrutas);
        this.tarifaComision = tarifaComision;
    }
	
	public Double getTarifaComision() {
        return tarifaComision;
    }

    public void setTarifaComision(Double tarifaComision) {
        this.tarifaComision = tarifaComision;
    }
	
	public Double getSalario(){
		return tarifaComision * getVentasBrutas();
    } 
    @Override
    public String toString() {
        return "Empleado{" + "Nombre=" + getPrimerNombre() + ", Apellido=" + getApellidoMaterno() + ", Seguridad Social=" + getNumeroSeguridadSocial() + ", Ventas brutas=" + getVentasBrutas() + ", Tarifa comisión=" + tarifaComision + ", Salario =" + getSalario() +  '}';
		
    }
 
    
    
}
 class EmpleadoBaseMasComision extends Empleado{
    private Double base;

    public EmpleadoBaseMasComision(String primerNombre, String apellidoMaterno, String numeroSeguridadSocial, Double ventasBrutas,Double base) {
        super(primerNombre, apellidoMaterno, numeroSeguridadSocial, ventasBrutas);
        this.base = base;
    }

    public Double getBase() {
        return base;
    }

    public void setBase(Double base) {
        this.base = base;
    }
	
	public Double getSalarioBase(){
		return  (10.00 * getVentasBrutas()) + base;
    }
    
    @Override
    public String toString() {
        return "Empleado{" + "Nombre=" + getPrimerNombre() + ", Apellido=" + getApellidoMaterno() + ", Seguridad Social=" + getNumeroSeguridadSocial() + ", Ventas brutas=" + getVentasBrutas() + ", Base=" + base + ", Salario =" + getSalarioBase() +  '}';
    }
	
    
    
}  

public class caso1{
   public static void main(String[] args){
	   EmpleadoPorComision empleadocomision = new EmpleadoPorComision("Juan","Paz","2548",2.00,10.00);    
	   String datos = empleadocomision.toString();
       System.out.println("Empleado por comisión");
	   System.out.println(datos);
	   
	   System.out.println("**************");
	   
	   EmpleadoBaseMasComision empleadobase = new EmpleadoBaseMasComision("Pedro","Roca","84216",5.00,400.00);    
	   String datosBase = empleadobase.toString();
       System.out.println("Datos del empleado base");
	   System.out.println(datosBase);
   }	   
   
}


